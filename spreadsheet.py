# pip install openpyxl
# while we are reading the file we should insert the values into the database table
from openpyxl import load_workbook

import db

def getIndex(list):
    return list.index('Nº receta')
    

def fill_table():
    wb = load_workbook(filename='lote.xlsx')
    first_sheet = wb.sheetnames[0]
    ws = wb[first_sheet]
    conn = db.create_connection("lote.db")
    db.delete_all_recipes(conn)
    #buscar el indice de la columna recetas
    values = list(ws.values)
    index=getIndex(values[0])
    values.pop(0)
    records = []
    #4 para pami y 0 para subsidio
    for row in values:
        if row[index] != None:
            records.append((row[index], "Falta controlar"))
    db.create_recipe(conn, records)
