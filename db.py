import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def create_recipe(conn, recipes):
    cur = conn.cursor()
    cur.executemany(
        ' INSERT INTO recipes(number, state) VALUES(?,?);', recipes)
    conn.commit()
    return cur.lastrowid


def select_recipe(conn, number, state):
    cur = conn.cursor()
    cur.execute(
        "SELECT * FROM recipes WHERE number LIKE ? AND state==?", ('%'+number+'%', state))
    row = cur.fetchall()
    return row


def select_recipe_by_state(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM recipes WHERE state is 'Falta controlar' ")
    row = cur.fetchall()
    return row


def select_all_recipes(conn):
    cur = conn.cursor()
    cur.execute("SELECT * FROM recipes ")
    row = cur.fetchall()
    return row


def delete_all_recipes(conn):
    sql = 'DELETE FROM recipes'
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()


def confirm_recipe(conn, numero, state):
    cur = conn.cursor()
    cur.execute("UPDATE recipes SET state = ? WHERE number = ?",
                (state, numero))
    conn.commit()


def main():
    database = r"lote.db"

    sql_create_recipes_table = """ CREATE TABLE IF NOT EXISTS recipes (
                                        id integer PRIMARY KEY,
                                        number integer,
                                        state string
                                    ); """

    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create recipes table
        create_table(conn, sql_create_recipes_table)

    else:
        print("Error! cannot create the database connection.")


if __name__ == '__main__':
    main()
