import tkinter as tk
from tkinter import filedialog, messagebox

import shutil
import os

import db
import spreadsheet

window = tk.Tk()
window.title("Test")
window.geometry('400x600')
window.counter = 0
counterLabel = tk.Label(window, text="0").pack()
conn = db.create_connection("lote.db")

# Create a new frame `frm_form` to contain the Label
# and Entry widgets for entering address information.
frm_form = tk.Frame(relief=tk.SUNKEN, borderwidth=3)
# Pack the frame into the window
frm_form.pack(ipadx=5, ipady=5)

# Loop over the list of field labels
# for idx, text in enumerate(labels):
# Create a Label widget with the text from the labels list
labelSearch = tk.Label(master=frm_form, text="Numero de receta/validación:")
# Create an Entry widget
entrySearch = tk.Entry(master=frm_form, width=50)
# Use the grid geometry manager to place the Label and
# Entry widgets in the row whose index is idx
labelSearch.grid(row=0, column=0, sticky="e")
entrySearch.grid(row=0, column=1)

frm_radios = tk.Frame()
frm_radios.pack(fill=tk.X, ipadx=5, ipady=5)
search_val = tk.StringVar()
R2 = tk.Radiobutton(frm_radios, text="Faltan Controlar",
                    variable=search_val, value="Falta controlar")
R3 = tk.Radiobutton(frm_radios, text="Controladas",
                    variable=search_val, value="Controlada")
R2.select()
R2.pack(side=tk.LEFT)
R3.pack(side=tk.LEFT)

frm_buttons = tk.Frame()
frm_buttons.pack(fill=tk.X, ipadx=5, ipady=5)


def handle_click(form, state):
    labels = db.select_recipe(conn, entrySearch.get(), state)
    if len(labels)==0:
        return False
    if len(labels)==1:
        db.confirm_recipe(conn, labels[0][1], "Controlada")
        return True
    else:
        for text in labels:
            R1 = tk.Radiobutton(form, text=text, variable=var, value=text[1])
            R1.deselect()
            R1.bind('<Return>', sel)
            R1.pack()
        form.winfo_children()[0].focus()
    return True

frm_result = tk.Frame(relief=tk.SUNKEN, borderwidth=3)
frm_result.pack()

#borra el contenido de la caja de resultados
def all_children(window):
    _list = window.winfo_children()
    for item in _list:
        if item.winfo_children():
            _list.extend(item.winfo_children())
    for item in _list:
        item.destroy()


def search(event):
    label.config(text="")
    all_children(frm_result)
    if not handle_click(frm_result, search_val.get()):
        messagebox.showwarning("Info", "Receta no encontrada N° "+entrySearch.get())
        entrySearch.delete(0, 'end')
        entrySearch.focus()



entrySearch.bind('<Return>', search)


def faltantes():
    all_children(frm_result)
    recipes = db.select_recipe_by_state(conn)
    if recipes:
        for text in recipes:
            L1 = tk.Label(frm_result, text=text)
            L1.pack()


def cargar_lote():
    filename = filedialog.askopenfilename(
        initialdir="/", title="Cargue el Lote", filetypes=(("XLSX", ".xlsx"), ("all files", "*.*")))
    if filename:
        shutil.copyfile(filename, "lote.xlsx")
        db.main()
        spreadsheet.fill_table()


btn_search = tk.Button(master=frm_buttons, text="Buscar")
btn_search.bind('<Button-1>', search)
btn_search.pack(side=tk.RIGHT, padx=10, ipadx=10)

btn_all = tk.Button(master=frm_buttons, text="Ver Faltantes",
                    command=faltantes)
btn_all.pack(side=tk.RIGHT, ipadx=10)
btn_load = tk.Button(master=frm_buttons, text="Cargar Lote",
                     command=cargar_lote)
btn_load.pack(side=tk.LEFT, ipadx=10)


var = tk.IntVar(value=0)

button_acept = tk.Button(
    master=window, text="Confirmar")


def confirm(event):
    state = ''
    if search_val.get() == "Controlada":
        state = "Falta controlar"
    else:
        state = "Controlada"
    db.confirm_recipe(conn, var.get(), state)
    button_acept.pack_forget()
    all_children(frm_result)
    label.config(text="Receta controlada!!!")
    window.counter += 1
    counterLabel['text'] = str(window.counter)
    entrySearch.delete(0, 'end')
    entrySearch.focus()


button_acept.bind('<Return>', confirm)


def sel(event):
    button_acept.pack()
    recipe = var.get()
    text = ''
    if recipe:
        text = "Seleccionaste la receta/autorización "
    else:
        text = "Todas las recetas han sido controladas!!"
    button_acept.focus()
    selection = text + str(recipe)
    label.config(text=selection)


label = tk.Label()
label.pack()


def on_closing():
    if messagebox.askokcancel("Cerrar", "Quiere cerrar el programa?"):
        db.delete_all_recipes(conn)
        if os.path.exists("lote.xlsx"):
            os.unlink('lote.xlsx')
        window.destroy()


window.protocol("WM_DELETE_WINDOW", on_closing)

window.mainloop()
