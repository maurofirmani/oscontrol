import tkinter as tk
from tkinter.messagebox import showinfo


class Window(tk.Tk):

    def __init__(self):
        tk.Tk.__init__(self)

        self.geometry('400x600')

        self.code = ''

        self.label = tk.Label(
            self, text="Escanee el Nro de receta o Cod. de Autorizacion")
        self.label.pack()

        self.bind('<Key>', self.get_key)

    def get_key(self, event):

        if event.char in '0123456789':
            self.code += event.char
            # print('>', self.code)
            self.label['text'] = self.code

        elif event.keysym == 'Return':
            # print('result:', self.code)
            showinfo('Code', self.code)

# --- main ---


win = Window()
win.mainloop()

# Window().mainloop()
